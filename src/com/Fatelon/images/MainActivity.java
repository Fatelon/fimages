package com.Fatelon.images;

import interfaces.SimpleDialogCallBack;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Random;

import util.CommonUtils;

import com.androidquery.AQuery;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import android.animation.Animator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends FragmentActivity {

	private ArrayList<String> objectNumbers = new ArrayList<String>(ProgectConstants.NUMBER_ITEM_IN_LIST);
	private CircularImageView questionImageView, answerImageView, emailButton, exitButton, promptButton;
	private CustomAnswerButton mLeftQuestion, mRightQuestion, mCenterQuestion;
	private LinearLayout scoreLayout;
	private Display display;
	private final DisplayMetrics mMetrics = new DisplayMetrics();
	private int mScreenWidth, mScreenHeight;
	private Context mContext;
	private AQuery aq;
	private Point mWindowSize = CommonUtils.getWindowSize();
	private Point questionImageSize = new Point(0, 0);
	private Point questionImagePosition = new Point(0, 0);
	private int currentItem = 0;
	private ImagesDataBaseHelper mImagesDataBaseHelper;
	private SQLiteDatabase mDatabase;
	private RelativeLayout mLoadScreenSaver;
	private String TAG = "myTag";
	private SharedPreferences mSettings; 
	private int questionAnswer;
	private int lastNumberOfDataBaseElements;
	private RelativeLayout answerBackground;
	private TextView scoreText;
	private int backgroundAnimateDuration = 400;
	private TextView answerTheQueation;
	private String[] answers = new String[3];
	private String currentPromptImageUrl;
	private boolean alreadyUsePrompt = false;
	private boolean alreadyShowEmptyBaseMessage = false;
	
	View.OnClickListener answerBackgroundClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			answerImageView.setVisibility(View.INVISIBLE);
			v.setOnClickListener(null);
			v.animate().setDuration(backgroundAnimateDuration)
			.alpha(0f)
			.setListener(new Animator.AnimatorListener() {
				@Override
				public void onAnimationStart(Animator animation) {
				}
				@Override
				public void onAnimationRepeat(Animator animation) {
				}
				@Override
				public void onAnimationEnd(Animator animation) {
					answerBackground.setVisibility(View.GONE);
//					scoreText.setTextColor(getResources().getColor(R.color.score_text_color_black));
					newQuestion();
				}
				@Override
				public void onAnimationCancel(Animator animation) {
				}
			});
		}
	};
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        aq = new AQuery(this);
        display = getWindowManager().getDefaultDisplay();
		display.getMetrics(mMetrics);
		mScreenWidth = mMetrics.widthPixels;
		mScreenHeight = mMetrics.heightPixels;
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initItems();
//        deleteCreatedDate();
//        clearDataBase();
        prepareBase();
    }
    
    private void initItems() {
    	mSettings = getSharedPreferences(ProgectConstants.mySharedPreferencesName, Context.MODE_PRIVATE);
    	questionImageView = (CircularImageView) findViewById(R.id.avatar_ImageView);
    	answerImageView = (CircularImageView) findViewById(R.id.question_answer_ImageView);
    	setImagesParameters();
    	questionImageView.setVisibility(View.VISIBLE);
    	setMiniButtonsParameters();
    	scoreText = (TextView)findViewById(R.id.score_text);
    	scoreText.setTypeface(null, Typeface.BOLD);
    	scoreText.setText(getUserScore() + "");
    	mLeftQuestion = (CustomAnswerButton)findViewById(R.id.left_question_Button);
    	mRightQuestion = (CustomAnswerButton)findViewById(R.id.right_question_Button);
    	mCenterQuestion = (CustomAnswerButton)findViewById(R.id.center_question_Button); 
    	mLeftQuestion.leftLocation();
    	mRightQuestion.rightLocation();
    	mCenterQuestion.centerLocation();
    	mLeftQuestion.setOnTouchListener(answerButtonOnTouch);
    	mRightQuestion.setOnTouchListener(answerButtonOnTouch);
    	mCenterQuestion.setOnTouchListener(answerButtonOnTouch);
    	answerTheQueation = (TextView)findViewById(R.id.answer_the_queation);
    	answerTheQueation.setY(questionImagePosition.y + questionImageSize.y 
    			+ getResources().getDimension(R.dimen.avatar_indent));
    	answerTheQueation.setTypeface(null, Typeface.BOLD);
    	mLoadScreenSaver = (RelativeLayout)findViewById(R.id.loading_RelativeLayout);
    	answerBackground = (RelativeLayout)findViewById(R.id.answer_background);
    	mImagesDataBaseHelper = new ImagesDataBaseHelper(this);
    	mDatabase = mImagesDataBaseHelper.getWritableDatabase();
    }
    
    private void setImagesParameters() {
    	questionImageSize = new Point(mScreenWidth / 2, mScreenWidth / 2);
    	questionImagePosition = new Point(mScreenWidth / 4, mScreenHeight / 3 - mScreenWidth / 4);
    	// set question image parameters.
    	questionImageView.setBorderColor(getResources().getColor(R.color.circular_border_white));
    	questionImageView.setBorderWidth(8);
    	questionImageView.setLayoutParams(new RelativeLayout.LayoutParams(questionImageSize.x, questionImageSize.y));
        questionImageView.setX(questionImagePosition.x);
        questionImageView.setY(questionImagePosition.y);
        // set answer image parameters.
        answerImageView.setBorderColor(getResources().getColor(R.color.circular_border_white));
        answerImageView.setBorderWidth(8);
        answerImageView.setLayoutParams(new RelativeLayout.LayoutParams(questionImageSize.x, questionImageSize.y));
        answerImageView.setX(questionImagePosition.x);
        answerImageView.setY(questionImagePosition.y);
//        answerImageView.setVisibility(View.GONE);
    }
    
    private void setMiniButtonsParameters() {
    	emailButton = (CircularImageView) findViewById(R.id.email_button);
    	exitButton = (CircularImageView) findViewById(R.id.exit_button);
    	promptButton = (CircularImageView) findViewById(R.id.prompt_button);
    	Point buttonsSize = new Point(mScreenWidth / 7, mScreenWidth / 7);
    	Point emailButtonPos = new Point(mScreenWidth / 7 - buttonsSize.x / 2, mScreenHeight / 25);
    	Point exitButtonPos = new Point(mScreenWidth * 6 / 7 - buttonsSize.x / 2, mScreenHeight / 25);
    	Point promptButtonPos = new Point(mScreenWidth * 6 / 7 - buttonsSize.x / 2, mScreenHeight / 3 + mScreenHeight / 25);
    	
    	aq.id(emailButton).image(R.drawable.email);
    	emailButton.setBorderColor(getResources().getColor(R.color.circular_border_white));
    	emailButton.setLayoutParams(new RelativeLayout.LayoutParams(buttonsSize.x, buttonsSize.y));
    	emailButton.setX(emailButtonPos.x);
    	emailButton.setY(emailButtonPos.y);
    	emailButton.setOnClickListener(emailClick);
    	
    	aq.id(exitButton).image(R.drawable.exit);
    	exitButton.setBorderColor(getResources().getColor(R.color.circular_border_white));
    	exitButton.setLayoutParams(new RelativeLayout.LayoutParams(buttonsSize.x, buttonsSize.y));
    	exitButton.setX(exitButtonPos.x);
    	exitButton.setY(exitButtonPos.y);
    	exitButton.setOnClickListener(exitClick);
    	
    	scoreLayout = (LinearLayout) findViewById(R.id.score_layout);
    	scoreLayout.setY(mScreenWidth / 8);
    	
    	aq.id(promptButton).image(R.drawable.prompt);
    	promptButton.setBorderColor(getResources().getColor(R.color.circular_border_white));
    	promptButton.setLayoutParams(new RelativeLayout.LayoutParams(buttonsSize.x, buttonsSize.y));
    	promptButton.setX(promptButtonPos.x);
    	promptButton.setY(promptButtonPos.y);
    	promptButton.setOnClickListener(promptClick);
    }
    
    private void prepareBase() {
    	lastNumberOfDataBaseElements = getObjectCountInDataBase();
    	if (lastNumberOfDataBaseElements < ProgectConstants.MIN_ITEM_COUNT) {
    		rechargeMyBase(true);
    	} else {
//    		getRandomObjectId();
    		newQuestion();
    		if (lastNumberOfDataBaseElements < ProgectConstants.MAX_SAVE_ITEMS) {
    			rechargeMyBase(false);
    		}
    	}
    }
    
    private void rechargeMyBase(final boolean changeQuestion) {
    	if (!CommonUtils.isNetworkAvailable(mContext)) {
    		mLoadScreenSaver.setVisibility(View.VISIBLE);
    		CommonUtils.showSimpleDilaog(getSupportFragmentManager(), 
    				noInternetCallback, getResources().getString(R.string.no_internet_dialog_title));
    		return;
    	}
    	ParseQuery<ParseObject> query = ParseQuery.getQuery(ProgectConstants.queryFamous);
    	lastNumberOfDataBaseElements = getObjectCountInDataBase();
    	int limit = Math.min(ProgectConstants.MAX_SAVE_ITEMS, 
    			ProgectConstants.MAX_SAVE_ITEMS - lastNumberOfDataBaseElements);
    	query.setLimit(limit);
    	query.whereGreaterThan("createdAt", new Date(getCreatedDate()));
        query.findInBackground(new FindCallback<ParseObject>() {
			@Override
			public void done(List<ParseObject> coursesList, ParseException e) {
				if (e == null) {
					if (coursesList.size() == 0) {
						if (alreadyShowEmptyBaseMessage) {
							resetData(changeQuestion);	
        				} else {
        					alreadyShowEmptyBaseMessage = true;
        					Log.e(TAG, "empty courses base");
//    						Toast.makeText(mContext, "empty server base", Toast.LENGTH_LONG);
    						new AlertDialog.Builder(mContext)
    					    .setMessage(getResources().getString(R.string.empty_base))
    					    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
    					        public void onClick(DialogInterface dialog, int which) { 
    					        	new Handler().postDelayed(new Runnable() {
    									@Override
    									public void run() {
    										resetData(changeQuestion);
    									}
    								}, 2000);
    					        }
    					     })
    					     .show();
        				}
						return;
					}
					Log.d(TAG, "courses list has - " + coursesList.size() + " elements");
					long timeInMilliseconds = -1;
					for (ParseObject course : coursesList) {
						java.util.Date createdAt = course.getCreatedAt();
						timeInMilliseconds = createdAt.getTime();
	                    addName(course.getObjectId());
	                }
					if (timeInMilliseconds > -1) {
						saveCreatedDate(timeInMilliseconds);
					}
//					getRandomObjectId();
					if (changeQuestion) {
						newQuestion();
					}
		        } else {
		        	Log.e(TAG, "e == null");
		        	deleteCreatedDate();
		        	rechargeMyBase(true);
		        	CommonUtils.showSimpleDilaog(getSupportFragmentManager(), 
	        				noInternetCallback, getResources().getString(R.string.no_server_answer_dialog_title));
		        }
			}
		});
    }
    
    private void resetData(boolean changeQuestion) {
    	deleteCreatedDate();
		if (changeQuestion) {
			mLoadScreenSaver.setVisibility(View.VISIBLE);
			clearDataBase();
		} 
		rechargeMyBase(changeQuestion);
    }
    
    private void alertForComment() {
    	new AlertDialog.Builder(mContext)
        .setMessage(getResources().getString(R.string.comment_text))
        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) { 
            	// do nothing
            }
         })
//        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) { 
//                // do nothing
//            }
//         })
        .setIcon(android.R.drawable.ic_dialog_alert)
         .show();
    }
    
    private void newQuestion() {
    	if (!CommonUtils.isNetworkAvailable(mContext)) {
    		mLoadScreenSaver.setVisibility(View.VISIBLE);
    		CommonUtils.showSimpleDilaog(getSupportFragmentManager(), 
    				noInternetCallback, getResources().getString(R.string.no_internet_dialog_title));
    		return;
    	}
    	Log.d(TAG, "generated new question");
    	try {
    		if (objectNumbers.size() == 0) {
    			getRandomObjectId();
    		}
    		if (objectNumbers.size() > 0) {
	    		String randomObjectId = objectNumbers.get(0);
	    		currentItem++;
	    		if (randomObjectId.equals("")) {
	    			Log.e(TAG, "randomObjectId in empty");
	    			return;
	    		}
	    		ParseQuery<ParseObject> query = ParseQuery.getQuery(ProgectConstants.queryFamous);
	    		query.getInBackground(randomObjectId, new GetCallback<ParseObject>() {
	    		    public void done(ParseObject object, ParseException e) {
	    		        if (e == null) {
	    		        	Log.d(TAG, "Ok, change view params..");
	    		        	changeViewsParams(object);
	    		        } else {
	    		            // something went wrong
	    		        	mLoadScreenSaver.setVisibility(View.VISIBLE);
	    		        	if (!CommonUtils.isNetworkAvailable(mContext)) {
	    		        		CommonUtils.showSimpleDilaog(getSupportFragmentManager(), 
	    		        				noInternetCallback, getResources().getString(R.string.no_internet_dialog_title));
	    		        		return;
	    		        	} else {
	    		        		CommonUtils.showSimpleDilaog(getSupportFragmentManager(), 
	    		        				noInternetCallback, getResources().getString(R.string.no_server_answer_dialog_title));
	    		        	}
	    		        }
	    		    }
	    		});
    		} else {
    			// ������ ������ - ���� �������� ��� ��� ����� ����?!
    			mLoadScreenSaver.setVisibility(View.VISIBLE);
    			Log.e(TAG, "empty objects list?!");
    			prepareBase();
    		}
	    	 
	    	if (objectNumbers.size() < ProgectConstants.MIN_ITEM_COUNT) {
	    		lastNumberOfDataBaseElements = getObjectCountInDataBase();
	    		Log.d(TAG, "neead recharge database and " +
	    				"lastNumberOfDataBaseElements = " + lastNumberOfDataBaseElements);
	    		rechargeMyBase(false);
	    	}
    	} catch (Throwable e) {
    		e.printStackTrace();
    		Log.e(TAG, "something went wrong in newQuestion()");
//    		Toast.makeText(mContext, "���-�� ����� �� ��� ��� ����� �������!!", Toast.LENGTH_LONG).show();
    	}
    }
    
    private void changeViewsParams(ParseObject curHero) {
    	try {
    		ParseFile postImage = curHero.getParseFile(ProgectConstants.IMAGE_FILE_1);
        	aq.id(questionImageView).image(postImage.getUrl());
        	currentPromptImageUrl = curHero.getParseFile(ProgectConstants.IMAGE_FILE_2).getUrl();
        	aq.id(answerImageView).image(curHero.getParseFile(ProgectConstants.IMAGE_FILE_ANSWER).getUrl());
        	alreadyUsePrompt = false;
        	ArrayList<Question> answersStrings = new ArrayList<Question>();
        	answersStrings.add(new Question(ProgectConstants.FIRST_Q));
        	answersStrings.add(new Question(ProgectConstants.SECOND_Q));
        	answersStrings.add(new Question(ProgectConstants.THIRD_Q));
        	answersStrings.get(curHero.getInt(ProgectConstants.ANSWER) - 1).trueAnswer = true;
        	int i = 2, j = 0;
        	while (i >= 0) {
        		int randomNumber = new Random().nextInt(i + 1);
        		if (answersStrings.get(randomNumber).trueAnswer) {
        			questionAnswer = j;
        		}
        		answers[j] = curHero.getString(answersStrings.remove(randomNumber).questionName);
        		i--;
        		j++;
        	}
        	mLeftQuestion.changeText(answers[0]);
        	mRightQuestion.changeText(answers[1]);
        	mCenterQuestion.changeText(answers[2]);
        	mLeftQuestion.setShadowFlag(true);
        	mRightQuestion.setShadowFlag(true);
        	mCenterQuestion.setShadowFlag(true);
        	// now buttons is available.
        	mLeftQuestion.setClickable(true);
        	mRightQuestion.setClickable(true);
        	mCenterQuestion.setClickable(true);
        	mLoadScreenSaver.setVisibility(View.GONE);
        	deleteRowInDataBase(objectNumbers.get(0));
        	Log.d(TAG, "remove objectNumbers = " + objectNumbers.get(0));
        	objectNumbers.remove(0);
        	Log.d(TAG, "objectNumbers size = " + objectNumbers.size());
	    } catch (Throwable e) {
			e.printStackTrace();
//			Toast.makeText(mContext, "���-�� ����� �� ��� ��� ����� ���������� layout!!", Toast.LENGTH_SHORT).show();
		}
    }
    
    private int getUserScore() {
		try {
			int createdAt = mSettings.getInt(ProgectConstants.userScore, 0);
	    	Log.d(TAG, "get user score = " + createdAt);
			return createdAt;
    	} catch(Throwable e) {
    		e.printStackTrace();
    	}
		return 0;
    }
    
    private void saveUserScore(int score) {
		try {
			Editor editor = mSettings.edit();
			editor.putInt(ProgectConstants.userScore, score);
			editor.apply();
			Log.i(TAG, "user scores is saved");
    	} catch(Throwable e) {
    		e.printStackTrace();
    	}
    }
    
    private long getCreatedDate() {
		try {
			long createdAt = mSettings.getLong(ProgectConstants.createdTimeName, 0);
	    	Log.d(TAG, "time created = " + createdAt);
			return createdAt;
    	} catch(Throwable e) {
    		e.printStackTrace();
    	}
		return 0;
    }
    
    private void saveCreatedDate(long time) {
		try {
			Editor editor = mSettings.edit();
			editor.putLong(ProgectConstants.createdTimeName, time);
			editor.apply();
			Log.i(TAG, "Created date is saved");
    	} catch(Throwable e) {
    		e.printStackTrace();
    	}
    }
    
    private void deleteCreatedDate() {
		try {
			mSettings = getSharedPreferences(ProgectConstants.mySharedPreferencesName, Context.MODE_PRIVATE);
	    	Editor editor = mSettings.edit();
			editor.remove(ProgectConstants.createdTimeName);
			editor.apply();
			Log.i(TAG, "Created date delete");
    	} catch(Throwable e) {
    		e.printStackTrace();
    	}
    }
    
    private void addName(String objectId) {
    	try {
    		ContentValues cv = new ContentValues();
    		cv.put(mImagesDataBaseHelper.OBJECT_ID, objectId);
    		mDatabase.insert(mImagesDataBaseHelper.TABLE_NAME, null, cv);
    		Log.i(TAG, "name is added");
    	} catch(Throwable e) {
    		e.printStackTrace();
    	}
    }
    
    private void deleteRowInDataBase(String objectId) {
    	try {
    		int count = mDatabase.delete(mImagesDataBaseHelper.TABLE_NAME, 
        			mImagesDataBaseHelper.OBJECT_ID + " = ?", new String[] {objectId});
        	Log.i(TAG, "Row is deleted = " + count);
    	} catch(Throwable e) {
    		e.printStackTrace();
    	}
    }
    
    private void clearDataBase() {
    	try {
    		synchronized(mDatabase) {
	    		mDatabase.delete(mImagesDataBaseHelper.TABLE_NAME, null, null); 
	        	Log.i(TAG, mImagesDataBaseHelper.TABLE_NAME + " is clear");
    		}
    	} catch(Throwable e) {
    		e.printStackTrace();
    	}
//    	deleteCreatedDate();
    }
    
    private int getObjectCountInDataBase() {
    	try {
    		Cursor cursor = mDatabase.rawQuery("SELECT  * FROM " + mImagesDataBaseHelper.TABLE_NAME, null);
        	Log.i(TAG, "getObjectCountInDataBase() return - " + cursor.getCount());
        	return cursor.getCount();
    	} catch(Throwable e) {
    		e.printStackTrace();
    	}
    	return 0;
    }
    
    /**
     * Get random object from famous database - mDatabase
     */
    public void getRandomObjectId() {
    	try {
    		Cursor cursor = mDatabase.query(mImagesDataBaseHelper.TABLE_NAME, 
        			new String[]{mImagesDataBaseHelper.OBJECT_ID}, 
        			null, 
        			null, 
        			null, 
        			null, 
        			"RANDOM() LIMIT " + ProgectConstants.NUMBER_ITEM_IN_LIST);
        	while (cursor.moveToNext()) {
        		String objId = cursor.getString(cursor.getColumnIndex(mImagesDataBaseHelper.OBJECT_ID));
        		if (objectNumbers.size() == 0 || objId != objectNumbers.get(0)) {
        			objectNumbers.add(objId);
        			Log.d("addItem", "" + cursor.getString(cursor.getColumnIndex(mImagesDataBaseHelper.OBJECT_ID)));
        		}
    		}
        	Log.d("addItem", "-------------------------");
        	lastNumberOfDataBaseElements = cursor.getCount();
    		cursor.close();
    	} catch(Throwable e) {
    		e.printStackTrace();
    	}
    }
    
    public void onClick(View view) {
    	// now buttons is not available.
    	mLeftQuestion.setClickable(false);
    	mRightQuestion.setClickable(false);
    	mCenterQuestion.setClickable(false);
    	answerBackground.setVisibility(View.VISIBLE);
    	answerImageView.setVisibility(View.VISIBLE);
    	answerBackground.animate()
    	.setDuration(backgroundAnimateDuration)
    	.alpha(1f)
    	.setListener(new Animator.AnimatorListener() {
			@Override
			public void onAnimationStart(Animator animation) {
			}
			@Override
			public void onAnimationRepeat(Animator animation) {
			}
			@Override
			public void onAnimationEnd(Animator animation) {
				answerBackground.setOnClickListener(answerBackgroundClickListener);
			}
			@Override
			public void onAnimationCancel(Animator animation) {
			}
		});
    	int userAnswer = 0;
    	switch (view.getId()) {
    	case R.id.left_question_Button:
    		userAnswer = 0;
    		break;
    	case R.id.right_question_Button:
    		userAnswer = 1;
    		break;
    	case R.id.center_question_Button:
    		userAnswer = 2;
    		break;
    	}
    	int userScore = getUserScore();
    	if (questionAnswer == userAnswer) {
    		userScore++;
    		scoreAnimate(userScore);
    		answerTheQueation.setText(getString(R.string.true_answer) + "\n" + answers[questionAnswer]);
    		Log.d(TAG, "true answer");
    	} else {
    		if (userScore > 0) {
	    		userScore--;
	    		scoreAnimate(userScore);
        	}
    		answerTheQueation.setText(getString(R.string.false_answer) + "\n" + answers[questionAnswer]);
    		Log.d(TAG, "wrong answer");
    	}
//    	newQuestion();
    }
    
    private void scoreAnimate(final int newScore) {
    	scoreLayout.animate()
    	.setDuration(backgroundAnimateDuration)
    	.scaleY(2.0f)
    	.scaleX(2.0f)
    	.setListener(new Animator.AnimatorListener() {
    		@Override
    		public void onAnimationStart(Animator animation) {
    		}
    		@Override
    		public void onAnimationRepeat(Animator animation) {
    		}
    		@Override
    		public void onAnimationEnd(Animator animation) {
    			saveUserScore(newScore);
        		scoreText.setText(newScore + "");
        		scoreLayout.animate()
            	.setDuration(backgroundAnimateDuration)
            	.scaleX(1.0f)
            	.scaleY(1.0f)
            	.setListener(null);
    		}
    		@Override
    		public void onAnimationCancel(Animator animation) {
    		}
    	});
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "stop all process. Well - it is end.");
        mDatabase.close();
        mImagesDataBaseHelper.close();
    }
    
    private SimpleDialogCallBack noInternetCallback = new SimpleDialogCallBack() {
		@Override
		public void okClick() {
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					prepareBase();
				}
			}, 2000);
		}

		@Override
		public void error() {
			// TODO Auto-generated method stub
		}
	};
	
	private View.OnTouchListener answerButtonOnTouch = new OnTouchListener() {
		
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			try {
				if (!v.isClickable()) {
					return false;
				}
				if(event.getAction() == MotionEvent.ACTION_DOWN ||
		        		event.getAction() == MotionEvent.ACTION_MOVE) {
					((CustomAnswerButton) v).setShadowFlag(false);
		        } else {
		        	((CustomAnswerButton) v).setShadowFlag(true);
		        }
			} catch (Throwable e) {
				e.printStackTrace();
			}
			
			return false;
		}
	};
	
	@Override
	public void onBackPressed() {
		new AlertDialog.Builder(mContext)
	    .setMessage(getResources().getString(R.string.really_exit))
	    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	        	MainActivity.this.finish();
	        }
	     })
	    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            // do nothing
	        }
	     })
	    .setIcon(android.R.drawable.ic_dialog_alert)
	     .show();
	}
	
	private View.OnClickListener exitClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			new AlertDialog.Builder(mContext)
		    .setMessage(getResources().getString(R.string.really_exit))
		    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		        	MainActivity.this.finish();
		        }
		     })
		    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // do nothing
		        }
		     })
		    .setIcon(android.R.drawable.ic_dialog_alert)
		     .show();
		}
	};
	
	private View.OnClickListener promptClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if (alreadyUsePrompt) {
				new AlertDialog.Builder(mContext)
    		    .setMessage(getResources().getString(R.string.already_use_prompt))
    		    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
    		        public void onClick(DialogInterface dialog, int which) { 
    		        	// do nothing
    		        }
    		     })
    		     .show();
			} else {
				new AlertDialog.Builder(mContext)
			    .setMessage(getResources().getString(R.string.really_prompt))
			    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			        	int userScore = getUserScore();
			        	if (userScore > 0) {
			        		aq.id(questionImageView).image(currentPromptImageUrl);
				    		userScore--;
				    		scoreAnimate(userScore);
				    		alreadyUsePrompt = true;
			        	} else {
			        		new AlertDialog.Builder(mContext)
			    		    .setMessage(getResources().getString(R.string.enough_stars))
			    		    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
			    		        public void onClick(DialogInterface dialog, int which) { 
			    		        	// do nothing
			    		        }
			    		     })
			    		     .show();
			        	}
			        }
			     })
			    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int which) { 
			            // do nothing
			        }
			     })
			     .show();
			}
			
		}
	};
	
	private View.OnClickListener emailClick = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			final EditText userInput = new EditText(mContext);  
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
			LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
			userInput.setLayoutParams(lp);
			builder.setTitle(R.string.send_dialog_title);
			builder.setView(userInput);
			builder.setPositiveButton(R.string.send_button_text, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		        	Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
		        	intent.setType("text/plain");
		        	intent.putExtra(Intent.EXTRA_SUBJECT, "Subject of email");
		        	intent.putExtra(Intent.EXTRA_TEXT, userInput.getText());
		        	intent.setData(Uri.parse("mailto:iknowimages@gmail.com")); // or just "mailto:" for blank
		        	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
		        	startActivity(intent);
		        }
		     });
			builder.setNegativeButton(R.string.cancel_button_text, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		        }
		     });
			AlertDialog dialog = builder.create();
	        dialog.show();
		}
	};
    
	public class Question {
		public String questionName;
		public boolean trueAnswer = false;
		public Question(String str) {
			questionName = str;
		}
	}
}