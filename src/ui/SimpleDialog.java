package ui;

import interfaces.SimpleDialogCallBack;

import com.Fatelon.images.CustomAnswerButton;
import com.Fatelon.images.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

public class SimpleDialog extends DialogFragment {

	private static SimpleDialogCallBack callBack = null;
	private static String title = "";
	private String mTag = "NoInternetDialog";
	
	public static SimpleDialog getSimpleDialog(SimpleDialogCallBack callBack, String title) {
		SimpleDialog.callBack = callBack;
		SimpleDialog.title = title;
		SimpleDialog f = new SimpleDialog();
        return f;
    }
	
	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        return new AlertDialog.Builder(getActivity())
            .setTitle(title)
            .setPositiveButton(getResources().getString(R.string.ok_button), okButtonClick)
            .create();
    }
	
	private DialogInterface.OnClickListener okButtonClick = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int whichButton) {
            try {
            	callBack.okClick();
            } catch (Throwable e) {
            	if (callBack != null) {
            		callBack.error();
            	}
                e.printStackTrace();
                Log.e(mTag, "accepted callback error in acceptedButtonClick");
            }
            dismiss();
        }
    };
	
    @Override
    public void dismiss() {
        super.dismiss();
        callBack = null; // in garbage
    }
}
