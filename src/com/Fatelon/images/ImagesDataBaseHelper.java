package com.Fatelon.images;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class ImagesDataBaseHelper extends SQLiteOpenHelper implements BaseColumns {

	public static final String TABLE_NAME = "famous_table";
	public static final String OBJECT_ID = "object_id";
	public static final String UID = "_id";
	private static final String DATABASE_NAME = "iknow_database.db";
	private static final int DATABASE_VERSION = 1;
	
	private static final String SQL_CREATE_ENTRIES = "CREATE TABLE "
            + TABLE_NAME + " (" + UID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + OBJECT_ID + " VARCHAR(255));";
	
	private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS "
            + TABLE_NAME;
	
	public ImagesDataBaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_ENTRIES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(SQL_DELETE_ENTRIES);
		onCreate(db);
	}

}
