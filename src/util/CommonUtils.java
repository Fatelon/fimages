package util;

import ui.SimpleDialog;
import interfaces.SimpleDialogCallBack;

import com.Fatelon.images.Application;
import com.Fatelon.images.R;

import android.content.Context;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

public class CommonUtils{

	private static Point sScreenSize = null;
	private final static String mTag = "CommonUtils";
	public static Context sAppContext = Application.getContext();
	
//	@Override
//    public void onCreate() {
//        super.onCreate();
//        sContext = getApplicationContext();
//        sScreenSize.x = sContext.getResources().getDisplayMetrics().widthPixels;
//		sScreenSize.y = sContext.getResources().getDisplayMetrics().heightPixels;
//    }
	
	public static Point getWindowSize() {
		if (sScreenSize == null) {
			if (sAppContext != null) {
				sScreenSize = new Point();
				WindowManager wm = (WindowManager) sAppContext.getSystemService(Context.WINDOW_SERVICE);
				Display display = wm.getDefaultDisplay();
				display.getSize(sScreenSize);
			} else {
				Log.e(mTag, "sContext is null");
			}
		}
		return sScreenSize;
	}
	
	public static boolean isNetworkAvailable(Context context) {
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	// Show new NoInternetDialog and remove old NoInternetDialog.
//    public static void showNoInternetDilaog(FragmentManager fragmentManager, SimpleDialogCallBack callBack) {
//        FragmentTransaction ft = fragmentManager.beginTransaction();
//        Fragment prev = fragmentManager.findFragmentByTag("NoInternetDialog");
//        if (prev != null) {
//            ft.remove(prev);
//        }
//        ft.addToBackStack(null);
//        DialogFragment newFragment = SimpleDialog.getNoInternetDialog(callBack, getResources().getString(R.string.no_internet_dialog_title));
//        newFragment.show(ft, "NoInternetDialog");
//    }
    
 // Show new SimpleDilaog and remove old SimpleDilaog.
    public static void showSimpleDilaog(FragmentManager fragmentManager, SimpleDialogCallBack callBack, String text) {
        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag("NoInternetDialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        DialogFragment newFragment = SimpleDialog.getSimpleDialog(callBack, text);
        newFragment.show(ft, "NoInternetDialog");
    }
	
}
