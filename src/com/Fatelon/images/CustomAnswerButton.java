package com.Fatelon.images;

import util.CommonUtils;

import com.androidquery.AQuery;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.provider.SyncStateContract.Constants;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CustomAnswerButton extends RelativeLayout {

	private AQuery aq;
	private TextView mText;
	private Point location = new Point(0, 0);
	private Point mWindowSize = CommonUtils.getWindowSize();
	private CircularImageView mBackgroundView;
	private PointF mCurrentViewSize = new PointF(0f, 0f);
	private float mBorderWidth = 0f;
	private float mShadowWidth = 0f;
	private int mBorderColor = getResources().getColor(R.color.circular_border_white);
	private int mBackgroundColor = getResources().getColor(R.color.circular_border_white);
	private boolean shadowFlag = true;
	
	public CustomAnswerButton(Context context) {
		super(context);
		init();
	}
	
	public CustomAnswerButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	public void changeLocation(Point newLocation) {
		this.location = newLocation;
		invalidate();
	}
	
	public void changeText(String newText) {
		mText.setText(newText);
		invalidate();
	}
	
	public void changeSize(PointF newSize) {
		mCurrentViewSize = newSize;
		invalidate();
	}
	
	public void changeBorderWidth(int newBorderWidth) {
		mBorderWidth = newBorderWidth;
		invalidate();
	}
	
	public void changeShadowWidth(float newShadowWidth) {
		mShadowWidth = newShadowWidth;
		invalidate();
	}
	
	public void setBorderColor(int color) {
		mBorderColor = color;
		invalidate();
	}
	
	public void setBackgroundColor(int color) {
		mBackgroundColor = color;
		invalidate();
	}
	
	public void setShadowFlag(boolean flag) {
		shadowFlag = flag;
		invalidate();
	}
	
	public void leftLocation() {
		Point newLocation = new Point(0, 0);
		newLocation.x = (int)(mWindowSize.x / 4 - mCurrentViewSize.x / 2);
		newLocation.y = (int)(mWindowSize.y * 7 / 10 - mCurrentViewSize.y / 2);
		changeLocation(newLocation);
	}
	
	public void rightLocation() {
		Point newLocation = new Point(0, 0);
		newLocation.x = (int)(mWindowSize.x * 3 / 4 - mCurrentViewSize.x / 2);
		newLocation.y = (int)(mWindowSize.y * 7 / 10 - mCurrentViewSize.y / 2);
		changeLocation(newLocation);
	}
	
	public void centerLocation() {
		Point newLocation = new Point(0, 0);
		newLocation.x = (int)(mWindowSize.x / 2 - mCurrentViewSize.x / 2);
		newLocation.y = (int)(mWindowSize.y * 7 / 10 + mCurrentViewSize.y / 2);
		changeLocation(newLocation);
	}
	
	private void init() {
		mShadowWidth = ProgectConstants.shadowWidth;
		changeSize(new PointF(mWindowSize.x * 2 / 5, mWindowSize.y / 7));
		initTextView();
		setWillNotDraw(false);
	}
	
	private void initTextView() {
		mText = new TextView(getContext());
		RelativeLayout.LayoutParams textLayoutParams = 
				new LayoutParams((int)mCurrentViewSize.x, (int)mCurrentViewSize.y);
        textLayoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        textLayoutParams.leftMargin = 10;
        textLayoutParams.rightMargin = 10;
		mText.setText("");
		mText.setTypeface(null, Typeface.BOLD);//|Typeface.ITALIC);
		mText.setTextColor(getResources().getColor(R.color.black_text_color));//Color.BLACK);
		mText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.button_text_size));
		mText.setGravity(Gravity.CENTER);
		mText.setLayoutParams(textLayoutParams);
		addView(mText);
		mText.setVisibility(View.VISIBLE);
		invalidate();
	}
	
	private PointF pointsSubtraction(PointF p, float value) {
		return new PointF(p.x - value, p.y - value);
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		final int desiredWSpec = MeasureSpec.makeMeasureSpec((int)mCurrentViewSize.x, MeasureSpec.EXACTLY);
		final int desiredHSpec = MeasureSpec.makeMeasureSpec((int)mCurrentViewSize.y, MeasureSpec.EXACTLY);
		setMeasuredDimension(desiredWSpec, desiredHSpec);
	}

	@Override
    public void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		// Draw border.
		Paint paint = new Paint();
		paint.setAntiAlias(true);
		paint.setColor(mBorderColor);
		if (shadowFlag) {
			paint.setShadowLayer(8.0f, 0.0f, 2.0f, Color.BLACK);
		} else {
			
		}
		
		setLayerType(LAYER_TYPE_SOFTWARE, paint);
		float ellipseIndent = mBorderWidth + mShadowWidth;
		PointF clearShadowSize = pointsSubtraction(mCurrentViewSize, mShadowWidth);
		RectF shadow = new RectF(mShadowWidth, mShadowWidth, clearShadowSize.x, clearShadowSize.y);
		canvas.drawOval(shadow, paint);
		
		// Draw ellipse in center. 
		paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(mBackgroundColor);
        PointF clearEllipseSize = pointsSubtraction(mCurrentViewSize, ellipseIndent);
        RectF ellipse = new RectF(ellipseIndent, ellipseIndent, clearEllipseSize.x, clearEllipseSize.y);
        canvas.drawOval(ellipse, paint);
        
        setX(location.x);
        setY(location.y);
//        initTextView();
	}
}
