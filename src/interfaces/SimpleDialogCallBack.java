package interfaces;

public interface SimpleDialogCallBack {
	public void okClick();
    public void error();
}
