package com.Fatelon.images;

import android.content.Context;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.SaveCallback;

public class Application extends android.app.Application {

	private static Context applicationContext = null;
	
	public Application() {
	
	}
	
	public static Context getContext() {
		return applicationContext;
	}
	
	@Override
	public void onCreate() {
		applicationContext = getApplicationContext();
		Parse.enableLocalDatastore(this);
		Parse.initialize(this, ProgectConstants.APPLICATION_ID, ProgectConstants.CLIENT_KEY);
		ParsePush.subscribeInBackground("", new SaveCallback() {
			  @Override
			  public void done(ParseException e) {
			    if (e == null) {
			      Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
			    } else {
			      Log.e("com.parse.push", "failed to subscribe for push", e);
			    }
			  }
			});
	}
}
