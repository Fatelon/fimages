package com.Fatelon.images;

import android.graphics.Point;

public class ProgectConstants {
	
	public static final String APPLICATION_ID = "0XF1anjA0XfZIEFAlM1SVet4UcpoWwqCx8POtKDX";
	public static final String CLIENT_KEY = "w8rzgGErOc5LbQXOwnXlIFVqK1PTkMmWW255yXC9";
	
	public static final String queryFamous = "images1";
	public static final String mySharedPreferencesName = "mysettings";
	public static final String createdTimeName = "lastCreatedAt";
	public static final String userScore = "userScore";
	public static final String FIRST_Q = "q1";
	public static final String SECOND_Q = "q2";
	public static final String THIRD_Q = "q3";
	public static final String ANSWER = "answer";
	
	public static final String IMAGE_FILE_1 = "image1";
	public static final String IMAGE_FILE_2 = "image2";
	public static final String IMAGE_FILE_ANSWER = "imageAnswer";
	
//	public static final Point avatarSize = new Point(mScreenWidth / 2, mScreenWidth / 2));
	
	public static final int MAX_SAVE_ITEMS = 5;
//	public static final int MAX_ITEM_COUNT_DIFFERENCE = 2;
	public static final int MIN_ITEM_COUNT = 2;
	
	public static final int NUMBER_ITEM_IN_LIST = 5;
	
	public final static float shadowWidth = 10;
	
}
